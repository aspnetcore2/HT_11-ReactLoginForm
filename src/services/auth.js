import axios from "axios";

export default function auth(username, password) {
  
    const urlReq = '/connect/token';
    const params = new URLSearchParams();

    params.append('client_id', 'm2m.client');
    params.append('client_secret', '511536EF-F270-4058-80CA-1C89C192F69A');
    params.append('grant_type', 'password');

    params.append('username', username); 
    params.append('password', password);

    axios.post(urlReq, params)
          .then((result) => {
            console.log(result.data.access_token);
          })
          .catch((error) => {
            console.error(error);
          })
}