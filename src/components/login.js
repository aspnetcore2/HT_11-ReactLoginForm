import React, { Component } from "react";
import auth from "../services/auth"

export default class Login extends Component{
    constructor(props) {
        super(props);
        this.submitLogin = this.submitLogin.bind(this);
    }

    submitLogin(){
        const userName = document.getElementById("userName").value;
        const password = document.getElementById("password").value;

        if (userName.length === 0 || password.length === 0)
        {
            console.log("User name and password are required");
            return;
        }

        auth(userName, password);
        
    }

    render() {
       return( 
        <div>
            <div>
                <label htmlFor="userName">User:</label>
                <input type="text" id="userName"/>
            </div>
            <div>
                <label htmlFor="password">Password:</label>
                <input type="password" id="password"/>
            </div>
            <div>
                <button type="button" onClick={this.submitLogin}>Login</button>
            </div>
        </div>
       );
    }
}